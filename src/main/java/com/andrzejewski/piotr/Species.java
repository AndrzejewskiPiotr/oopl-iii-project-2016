package com.andrzejewski.piotr;

public abstract class Species extends Animals implements CanEat {

    private static int nextId = 1;

    private int id;
    {
        id = nextId;
        nextId++;
    }

    public Species(String name, String age, String species, boolean canEat) throws InterruptedException{
        super(name, age, species, canEat);
    }

    public void description(){
        System.out.println("Name animal: " + getName());
        System.out.println("Age: " + getAge());
        System.out.println("Species: " + getSpecies());
        System.out.println("Got eat: " + getCanEat());
    }

    @Override
    public String toString() {

        return super.toString();
    }

    abstract void changeFly();

    public void checkAnimalCanEatOnHisOwn() {
        if (getCanEat()) {
            setCanEat(false);
        } else if (!getCanEat()) {
            setCanEat(true);
        }
        System.out.println("Status changed: " + getName() + " got eat: " + getCanEat());
    }

    public int getId() {

        return id;
    }
}