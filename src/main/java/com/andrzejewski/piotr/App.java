package com.andrzejewski.piotr;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class App
{
    public static void main( String[] args )
    {
        Set<Animals> BirdSet = new HashSet<Animals>();
        Set<Animals> WildcatSet = new HashSet<Animals>();
        HashMap<Animals, String> hashMap = new HashMap<Animals, String>();
        Species[] bird = new Species[5];
        Species[] wildcat = new Species[5];

        try {
            bird[0] = new Bird("Bartosz", "3 months", "bird", true, false);
            bird[1] = new Bird("Fabianski", "6 months", "bird", false, false);
            bird[2] = new Bird("Robert", "11 months", "bird", false, true);
            bird[3] = new Bird("Damian", "3 months", "bird", true, true);
            bird[4] = new Bird("Adam", "1 months", "bird", false, false);
            wildcat[0] = new Wildcat("Ryszard", "2 years", "wildcat", true);
            wildcat[1] = new Wildcat("Piotr", "4 months", "wildcat", true);
            wildcat[2] = new Wildcat("Roman", "7 month", "wildcat", false);
            wildcat[3] = new Wildcat("łysy", "2 years", "wildcat", true);
            wildcat[4] = new Wildcat("Rob", "11 month", "wildcat", false);
        }
        catch (InterruptedException e) {
            e.getStackTrace();
        }

        try {
            for (Animals s : bird) {
                BirdSet.add(s);
            }
            for (Animals s : wildcat) {
                WildcatSet.add(s);
            }
            for (int i = 0; i < 5; i++) {
                hashMap.put(bird[i], "b" + bird[i].getId());
            }
            for (int i = 0; i < 5; i++) {
                hashMap.put(wildcat[i], "w" + wildcat[i].getId());
            }
        }catch (Exception e) {
             e.getStackTrace();
        }
        System.out.println("Zoo Piotr 'PAJAC' Andrzejewski");
        System.out.println("Birds: " + BirdSet.size());
        System.out.println("Wildcats: " + WildcatSet.size());
        System.out.println();

        wildcat[0].checkAnimalCanEatOnHisOwn();
        wildcat[1].changeFly();
        bird[0].changeFly();

        for (Map.Entry<Animals, String> entry: hashMap.entrySet()) {
            System.out.println(entry.getKey() + "\nID: " + entry.getValue());
        }
    }
}
