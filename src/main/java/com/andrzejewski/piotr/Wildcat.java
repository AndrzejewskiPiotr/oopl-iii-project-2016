package com.andrzejewski.piotr;

    public class Wildcat extends Species {

        public Wildcat(String name, String age, String species, boolean canEat) throws InterruptedException {
        super(name, age, species, canEat);
    }

    @Override
    public void changeFly(){
        System.out.println(getName() + " can't fly!");
    }
}