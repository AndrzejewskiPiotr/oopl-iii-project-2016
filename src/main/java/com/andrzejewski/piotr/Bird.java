package com.andrzejewski.piotr;

public class Bird extends Species implements CanFly, CanEat {
    private boolean canFly;

    public Bird(String name, String age, String species, boolean canEat, boolean canFly) throws InterruptedException {
        super(name, age, species, canEat);
        this.canFly = canFly;
    }

    public void changeFly()
    {
        checkAnimalManageToFly();
    }

    @Override
    public String toString() {

        return super.toString() + "\n Can fly: " + canFly;
    }

    @Override

    public void description(){
        super.description();
        checkAnimalManageToFly();
        System.out.println();
    }
    public boolean getCanFly(){
        return canFly;
    }

    public void setCanFly(boolean canFly) {

        this.canFly = canFly;
    }

    public void checkAnimalManageToFly(){
        if (getCanFly()) {
            setCanFly(false);
        } else if (!getCanFly()) {
            setCanFly(true);
        }
        System.out.println("Status changed: " + getName() + " can fly: " + getCanFly());
    }
}
