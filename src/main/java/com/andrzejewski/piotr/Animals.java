package com.andrzejewski.piotr;

public class Animals {
    private String name;
    private String age;
    private String species;
    private boolean canEat;

    public Animals(String name, String age, String species, boolean canEat) throws InterruptedException{
        this.name = name;
        this.age = age;
        this.species = species;
        this.canEat = canEat;
    }

    @Override
    public String toString() {
        return "\n\nName: " + name + "\nAge: " + age + "\nSpecies: " + species + "\nCan eat: " + canEat;
    }

    public String getName(){

        return name;
    }

    public String getAge() {

        return age;
    }

    public String getSpecies() {

        return species;
    }

    public boolean getCanEat() {

        return canEat;
    }

    public void setCanEat(boolean canEat) {

        this.canEat = canEat;
    }
}
