package com.andrzejewski.piotr;

public interface CanEat {
    void checkAnimalCanEatOnHisOwn();
}
